#!/usr/bin/python
# -*- coding: utf-8 -*-
# Python 3
#https://python.jpvweb.com/python/mesrecettespython/doku.php?id=cx_freeze
#python setup.py build


from cx_Freeze import setup, Executable

exe=Executable(
        script="BoursoParse.py",
        #base="Win32Gui",                                    #Appli Windows avec fenetre
        icon="Boursorama.ico",                                  #Icone du fichier .exe
        targetName="BoursoParse",                            #Non du fichier .exe
        copyright="SDY"                                #Copyright du fichier
        )
includefiles=["Release.txt"]
includes=[]
excludes=[]
#excludes=["pydoc_data","asyncio","chardet","email","certifi","urllib","multiprocessing","unittest","idna","concurrent","ctypes","distutils","email","html","lib2to3","test","unittest","xml","xmlrpc"]
packages=[]
#"encodings","http",
#"encodings",

setup(
        name = "BoursoParse.exe",
        version = "1.4.2",
        description = "Outil de mise à jour des Performances des fonds Boursorama",
        author = "SDY",
        options = {'build_exe': {'excludes':excludes,'packages':packages,'include_files':includefiles}},
        executables = [exe]
        )
