

# "python_interpreter": "D:/Users/sdary/AppData/Local/Programs/Python/Python37/python.exe"   #
# -*- coding: utf-8 -*- 																	 #
#!/usr/bin/env python3																		 #
##############################################################################################
##############################################################################################
##############################################################################################
#Import des modules externes :
#Verification de la semaine.
#Option sur le log
#table rank, null à la in V Sqlite 3.30 : https://learnsql.com/blog/how-to-order-rows-with-nulls/

# Import libraries
import requests
import urllib.request
import time
from bs4 import BeautifulSoup
import sqlite3
import os.path
import datetime
import logging
from logging.handlers import RotatingFileHandler
from configparser import ConfigParser
import os

version='1.4.2'
#1.3.1 : Génération page web avec lien vers fonds
#1.2.2 : génération page web avec lien + Elig,... 
#1.3.3 : Correction DispoBq=7 & table Rank
#1.4.0 : MajPython 3.9 et SQLIte - gestion des null & - dans classement rank
#1.4.1 : Log si page innexistante
#1.4.2 : Ajout d'un message en fin de log en cas de Warning

#Gestion du fichier de configuration
config = ConfigParser()
try:
    config.read('BoursoParse.ini','utf-8')
except (Error, Exception) as e:
    logging.critical('Oops, une erreur dans votre fichier de conf : %s', e)
#   print('Oops, une erreur dans votre fichier de conf :', e)
    #messagebox.showerror("Error", "Oops, une erreur dans votre fichier de conf"+"\n"+str(e))
    sys.exit(1)

#Gestion du fichier de log
try:
    FileLog=config.get('GENERAL', 'FILELOG')
    StreamLog=config.get('GENERAL', 'STREAMLOG')
except:
    #logging.Error("Oops, la section n'a pas étée trouvée dans le fichier ini : %s", e)
    logging.error("Oops, la section recherchée n'a pas étée trouvée dans le fichier ini : ")
    FileLog='INFO'
    StreamLog='INFO'

logger = logging.getLogger()
logger.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s %(levelname)s:%(message)s', datefmt='%d/%m/%Y %H:%M:%S')
file_handler = RotatingFileHandler('log.txt', 'a', 100000, 1,encoding = 'utf-8')
file_handler.setLevel(FileLog)
file_handler.setFormatter(formatter)
logger.addHandler(file_handler)

stream_handler = logging.StreamHandler()
stream_handler.setLevel(StreamLog)               #INFO ou DEBUG
logger.addHandler(stream_handler)


#Affichage de la version et de l'utilisateur dans la console et le fichier de log quelque soit le niveau
stream_handler.setLevel('INFO')
file_handler.setLevel('INFO')

logging.info("Execution de l'application "+version+" par "+os.getlogin()+'LogFile : '+FileLog+"...")


stream_handler.setLevel(StreamLog)
file_handler.setLevel(FileLog)

logging.debug('Niveau de log du fichier : '+FileLog)

#Variable Globale
global GlobalWarning
GlobalWarning = 0 #Gestion des messages Warninn pour ouverture du log auto si présent



def _Recup_Bourso(BRSIdx):

    '''Récupération de la page web'''
    ##########################################

    #Variable Globale
    global GlobalWarning  #Gestion des messages Warninn pour ouverture du log auto si présent


    # Set the URL you want to webscrape from
    url = 'https://www.boursorama.com/bourse/opcvm/cours/'+str(BRSIdx).rstrip('\r\n')
    
    #--------------------------------------------#
    logging.debug('Url : %s', url)
    #--------------------------------------------#

    # Connect to the URL
    time.sleep(10)
    response = requests.get(url)
    logging.debug('Status de la page : '+str(response.status_code))
    logging.debug('URL finale : '+str(response.request.url))

    #Vérification que la page existe
    if 'recherche' in response.request.url:
        logging.warning('ATTENTION, la page n''a pas été trouvée : '+url)
        GlobalWarning=1
    
    # Parse HTML and save to BeautifulSoup object¶
    soup = BeautifulSoup(response.text, "html.parser")
    
    #print('*******Test*********')
    #divs=soup.find_all('th',   {'class' : 'c-table__title'})
    fonds=soup.find_all('div', {'class' : ['c-fund-performances']})
    for fond in fonds:#'                                        Performances du fonds                                    </span>'):
        #rows=soup.find_all('td' , {'class' : ['c-table__cell c-table__cell--dotted c-table__cell--positive','c-table__cell c-table__cell--dotted c-table__cell--negative','c-table__cell c-table__cell--dotted']} ) #Récupération des valeurs )
        temp = fond.find_all('td' , {'class' : ['c-table__cell c-table__cell--dotted c-table__cell--positive','c-table__cell c-table__cell--dotted c-table__cell--negative','c-table__cell c-table__cell--dotted']} ) #Récupération des valeurs 
        #print('////////////////////////////')
        #for temp in temps:
           #print(temp)




        '''Récupération des valeurs'''
        ##########################################
        #temp = soup.find_all('td' , {'class' : ['c-table__cell c-table__cell--dotted c-table__cell--positive','c-table__cell c-table__cell--dotted c-table__cell--negative','c-table__cell c-table__cell--dotted']} ) #Récupération des valeurs 
        try:
            v1J=str(temp[21]).replace('<td class="c-table__cell c-table__cell--dotted c-table__cell--positive">','').replace('</td>','').replace('<td class="c-table__cell c-table__cell--dotted c-table__cell--negative">','').replace('<td class="c-table__cell c-table__cell--dotted">','')
            v1S=str(temp[22]).replace('<td class="c-table__cell c-table__cell--dotted c-table__cell--positive">','').replace('</td>','').replace('<td class="c-table__cell c-table__cell--dotted c-table__cell--negative">','').replace('<td class="c-table__cell c-table__cell--dotted">','')
            v1M=str(temp[23]).replace('<td class="c-table__cell c-table__cell--dotted c-table__cell--positive">','').replace('</td>','').replace('<td class="c-table__cell c-table__cell--dotted c-table__cell--negative">','').replace('<td class="c-table__cell c-table__cell--dotted">','')
            v6M=str(temp[24]).replace('<td class="c-table__cell c-table__cell--dotted c-table__cell--positive">','').replace('</td>','').replace('<td class="c-table__cell c-table__cell--dotted c-table__cell--negative">','').replace('<td class="c-table__cell c-table__cell--dotted">','')
            v1A=str(temp[25]).replace('<td class="c-table__cell c-table__cell--dotted c-table__cell--positive">','').replace('</td>','').replace('<td class="c-table__cell c-table__cell--dotted c-table__cell--negative">','').replace('<td class="c-table__cell c-table__cell--dotted">','')
            v3A=str(temp[26]).replace('<td class="c-table__cell c-table__cell--dotted c-table__cell--positive">','').replace('</td>','').replace('<td class="c-table__cell c-table__cell--dotted c-table__cell--negative">','').replace('<td class="c-table__cell c-table__cell--dotted">','')
            v5A=str(temp[27]).replace('<td class="c-table__cell c-table__cell--dotted c-table__cell--positive">','').replace('</td>','').replace('<td class="c-table__cell c-table__cell--dotted c-table__cell--negative">','').replace('<td class="c-table__cell c-table__cell--dotted">','')
            v10A=str(temp[28]).replace('<td class="c-table__cell c-table__cell--dotted c-table__cell--positive">','').replace('</td>','').replace('<td class="c-table__cell c-table__cell--dotted c-table__cell--negative">','').replace('<td class="c-table__cell c-table__cell--dotted">','')


            #--------------------------------------------#
            logging.debug('VJ1 : '+str(v1J))
            logging.debug('v1S : '+str(v1S))
            logging.debug('v1M : '+str(v1M))
            logging.debug('v6M : '+str(v6M))
            logging.debug('v1A : '+str(v1A))
            logging.debug('v3A : '+str(v3A))
            logging.debug('v5A : '+str(v5A))
            logging.debug('v10A : '+str(v10A))



            #--------------------------------------------#
            # print(str(vtitre[12]).replace('<th class="c-table__title">','').replace('</th>','')+': '+v1J.strip())
            # print(str(vtitre[13]).replace('<th class="c-table__title">','').replace('</th>','')+': '+v1S.strip())
            # print(str(vtitre[14]).replace('<th class="c-table__title">','').replace('</th>','')+': '+v1M.strip())
            # print(str(vtitre[15]).replace('<th class="c-table__title">','').replace('</th>','')+': '+v6M.strip())
            # print(str(vtitre[16]).replace('<th class="c-table__title">','').replace('</th>','')+': '+v1A.strip())
            # print(str(vtitre[17]).replace('<th class="c-table__title">','').replace('</th>','')+': '+v3A.strip())
            # print(str(vtitre[18]).replace('<th class="c-table__title">','').replace('</th>','')+': '+v5A.strip())
            # print(str(vtitre[19]).replace('<th class="c-table__title">','').replace('</th>','')+': '+v10A.strip())

            '''Récupération des Titres de colonnes'''
            ##########################################
            #if FileLog =='logging.INFO' or StreamLog=='logging.INFO' or FileLog=='logging.DEBUG' or  StreamLog=='logging.DEBUG':
                    
            vtitre = soup.find_all('th',   {'class' : 'c-table__title'})
            #print(str(vtitre[12]).replace('<th class="c-table__title">','').replace('</th>',''))
            #print(str(vtitre[13]).replace('<th class="c-table__title">','').replace('</th>',''))
            #print(str(vtitre[14]).replace('<th class="c-table__title">','').replace('</th>',''))
            #print(str(vtitre[15]).replace('<th class="c-table__title">','').replace('</th>',''))
            #print(str(vtitre[16]).replace('<th class="c-table__title">','').replace('</th>',''))
            #print(str(vtitre[17]).replace('<th class="c-table__title">','').replace('</th>',''))
            #print(str(vtitre[18]).replace('<th class="c-table__title">','').replace('</th>',''))
            #print(str(vtitre[19]).replace('<th class="c-table__title">','').replace('</th>',''))
            #############################


            logging.info(str(vtitre[12]).replace('<th class="c-table__title">','').replace('</th>','')+': '+v1J.strip())
            logging.info(str(vtitre[13]).replace('<th class="c-table__title">','').replace('</th>','')+': '+v1S.strip())
            logging.info(str(vtitre[14]).replace('<th class="c-table__title">','').replace('</th>','')+': '+v1M.strip())
            logging.info(str(vtitre[15]).replace('<th class="c-table__title">','').replace('</th>','')+': '+v6M.strip())
            logging.info(str(vtitre[16]).replace('<th class="c-table__title">','').replace('</th>','')+': '+v1A.strip())
            logging.info(str(vtitre[17]).replace('<th class="c-table__title">','').replace('</th>','')+': '+v3A.strip())
            logging.info(str(vtitre[18]).replace('<th class="c-table__title">','').replace('</th>','')+': '+v5A.strip())
            logging.info(str(vtitre[19]).replace('<th class="c-table__title">','').replace('</th>','')+': '+v10A.strip())

            #Retourne les variations dans l'ordre sans les % et tabulations
            return(v1J.strip().replace('%',''),v1S.strip().replace('%',''),v1M.strip().replace('%',''),v6M.strip().replace('%',''),v1A.strip().replace('%',''),v3A.strip().replace('%',''),v5A.strip().replace('%',''),v10A.strip().replace('%',''))
        except IndexError:
            #Gestion des valeurs innexistante chez Boursorama
            logging.warning('Pas de tableau dans la page : '+BRSIdx)
            GlobalWarning = 1
            return None

    #################################

def update_stats(conn, task):
    """
    update 1Jan, Sem, Mois, 6Mois, 1An, 3ans, 5ans, 10ans  of a stats
    :param conn:
    :param task:
    :return: project id
    """

    sql = ''' UPDATE stats2
              SET Jan1 = ?,
                  Sem = ? ,
                  Mois = ?,
                  Mois6 = ?,
                  An1 = ?,
                  ans3 = ?,
                  ans5 = ?,
                  ans10= ?
              WHERE  ISIN = ? and  Wk= ?'''
    cur = conn.cursor()
    cur.execute(sql, task)
    logging.debug(sql.replace('?','%s') % task)
    #logging.debug('Update2 : '+str(task))
    conn.commit()


#################################
def insert_stats(conn, task):
    """
    Insert Wk, ISIN  in stats
    :param conn:
    :param task:
    """
    sql = ''' insert into stats2 (Wk,ISIN)
                            VALUES( ? , ? )'''
    cur = conn.cursor()
    cur.execute(sql, task)
    logging.debug(sql.replace('?','%s') % task)
    conn.commit()

###########################################
'''Partie principale'''
'''Connexion a la base SQLITE'''
##########################################

db_path=config.get('GENERAL', 'BD')
logging.info('Utilisation de la BD : '+db_path)
try:
    con = sqlite3.connect('file:'+db_path+'?mode=rw', uri=True)
except sqlite3.Error as er:
    #print('SQLite error: %s' % (' '.join(er.args)))
    #print("Exception class is: ", er.__class__)
    #print('SQLite traceback: ')
    #exc_type, exc_value, exc_tb = sys.exc_info()
    #print(traceback.format_exception(exc_type, exc_value, exc_tb))
    logging.critical('Ouverture de la BDD impossible : '+db_path)


with sqlite3.connect(db_path) as db:
    conn = sqlite3.connect('file:'+db_path+'?mode=rw', uri=True)
    conn.text_factory = sqlite3.OptimizedUnicode
    #logging.debug('Connexion : '+str(db))




#t = ('MP-97',) #pour Test
#t = ('0P00018KOU/',)
t = "('0P000125M/','MP-202174/')"

cur = conn.cursor()
#logging.debug('Connexion : '+str(cur))


#Version de la base#
sql='select sqlite_version();'
for row in cur.execute(sql):
    logging.info('Sqllite Version : '+str(row[0]))

#Ouverture de la page Web
webpage=fichier = open("fonds.htm", "w")    
webpage.write('<html>\n<body>\n<table>\n<thead>\n<style>\ntable, th, td {\n\t  border: 1px solid black;\n\tpadding:10px;\n}\n</style>\n<tr>\n\t<th>Lien</th><th>ISIN</th><th>Elig</th><th>DispoBq</th><th>DetBq</th>\n</tr>\n</thead>\n<tbody>\n' )

#Pour chaque ligne de la table valeur alimentation de stats
#sql='''SELECT BRSIdx,ISIN,ValNom, case Elig   when 1 then 'PEA'   WHEN 3 then 'PME'   else '-'    end ELIG,case DispoBq       WHEN 1 then 'BRS'       WHEN 2 then 'FOR'       WHEN 3 then 'BRS & FOR'     WHEN 4 then 'EZB'       WHEN 5 then 'BRS & EZB'     WHEN 6 then 'FOR & EZB'  WHEN 7 then 'BRS & FOR & EZB'    else '-'        end DispoBq,case DispoBq        WHEN 1 then 'BRS'       WHEN 2 then 'FOR'       WHEN 3 then 'BRS & FOR'     WHEN 4 then 'EZB'       WHEN 5 then 'BRS & EZB'     WHEN 6 then 'FOR & EZB'   WHEN 7 then 'BRS & FOR & EZB'   else '-'        end DetBq   FROM ValList where BRSIdx in'''+str(t)+''' order by ValNom'''
sql='''SELECT BRSIdx,ISIN,ValNom, case Elig   when 1 then 'PEA'   WHEN 3 then 'PME'   else '-'    end ELIG,case DispoBq       WHEN 1 then 'BRS'       WHEN 2 then 'FOR'       WHEN 3 then 'BRS & FOR'     WHEN 4 then 'EZB'       WHEN 5 then 'BRS & EZB'     WHEN 6 then 'FOR & EZB'  WHEN 7 then 'BRS & FOR & EZB'    else '-'        end DispoBq,case DispoBq        WHEN 1 then 'BRS'       WHEN 2 then 'FOR'       WHEN 3 then 'BRS & FOR'     WHEN 4 then 'EZB'       WHEN 5 then 'BRS & EZB'     WHEN 6 then 'FOR & EZB'   WHEN 7 then 'BRS & FOR & EZB'   else '-'        end DetBq   FROM ValList order by ValNom'''
#logging.debug('Requete : '+str(sql))

if cur.execute(sql).fetchall==0:
    logging.warning('Pas de fonds a traiter dans la table ValList : '+str(cur.execute(sql).fetchall))
    GlobalWarning = 1


for row in cur.execute(sql):
    #print(row[0]+'-'+row[1])
    logging.info("Traitement de  : "+str(row[0])+'-'+str(row[1])+'-'+str(row[2]))
    webpage.write('<p><tr>\n\t<td><a href="https://www.boursorama.com/bourse/opcvm/cours/'+str(row[0])+'"">'+str(row[2])+'</a></td>\n\t<td>'+str(row[1])+'</td>\n\t<td>'+str(row[3])+'</td>\n\t<td>'+str(row[4])+'</td>\n\t<td>'+str(row[5])+'</td>\n</tr></p>\n' )

    #logging.info(row[0]+'-'+row[1])
    vRestultat=_Recup_Bourso(row[0])
    
    #Si le tableau retourné est vide passe au suivant
    if vRestultat == None:
        continue


    #Alimentation de la BDD
    vWeek=datetime.datetime.now().strftime("%Y%V")[-4:]
    tt=(row[1],vWeek)
    test=conn.execute('SELECT Wk,ISIN FROM Stats2 WHERE ISIN=? and Wk=?', tt)

    #Test l'existance de la ligne ISIN-Semaine
    if len(test.fetchall())!=0:
        #print('Update : '+row[1]+'-'+vWeek)
        logging.info('Update : '+row[1]+'-'+row[0]+'-'+vWeek)
        update_stats(conn, (vRestultat[0],vRestultat[1],vRestultat[2],vRestultat[3],vRestultat[4],vRestultat[5],vRestultat[6],vRestultat[7],row[1],vWeek))
    else :
        #print('Insert : '+row[1]+'-'+vWeek)
        logging.info('Insert : '+row[1]+'-'+row[0]+'-'+vWeek)
        insert_stats(conn, (vWeek,row[1]))
        update_stats(conn, (vRestultat[0],vRestultat[1],vRestultat[2],vRestultat[3],vRestultat[4],vRestultat[5],vRestultat[6],vRestultat[7],row[1],vWeek))

#Création de la table Rank
#####Vérification de l'existance de la table rank et création
sql=''' SELECT count(name) FROM sqlite_master WHERE type='table' AND name='rank' '''
cur.execute(sql)
if cur.fetchone()[0]!=1:
    sql=''' create table rank ( '''
    sql=sql+''' RJan1 INTEGER,RSem INTEGER,RMois INTEGER,RMois6 INTEGER,RAn1 INTEGER,RAns3 INTEGER,RAns5 INTEGER,RAns10 INTEGER,'''
    sql=sql+''' Wk integer(4) NOT NULL,ISIN TEXT(12) NOT NULL,Jan1 real(5,2),Sem real(5,2),Mois real(5,2),mois6 real(5,2),An1 r(5,2),Ans3 real(5,2),Ans5 real(5,2),Ans10 real(5,2),'''
    sql=sql+''' PRIMARY KEY ("Wk", "ISIN") ON CONFLICT FAIL,'''
    sql=sql+''' CONSTRAINT "ISIN" FOREIGN KEY ("ISIN") REFERENCES "ValList" ("ISIN") ON DELETE CASCADE ON UPDATE CASCADE );'''

    cur.execute(sql)
    logging.info('Création de la table rank')

    sql='''insert into rank  select  row_number() over( PARTITION by Wk order by case Jan1 when '-' then null else Jan1 end desc NULLS last) as RJan1,'''
    sql=sql+''' row_number() over( PARTITION by Wk order by case Sem when '-' then null else Sem end desc NULLS last) as RSem,'''
    sql=sql+''' row_number() over( PARTITION by Wk order by case Mois when '-' then null else Mois end desc NULLS last) as RMois,'''
    sql=sql+''' row_number() over( PARTITION by Wk order by case mois6 when '-' then null else mois6 end desc NULLS last) as RMois6,'''
    sql=sql+''' row_number() over( PARTITION by Wk order by case An1 when '-' then null else An1 end desc NULLS last) as RAn1,'''
    sql=sql+''' row_number() over( PARTITION by Wk order by case Ans3 when '-' then null else Ans3 end desc NULLS last) as RAns3,'''
    sql=sql+''' row_number() over( PARTITION by Wk order by case Ans5 when '-' then null else Ans5 end desc NULLS last) as RAns5,'''
    sql=sql+''' row_number() over( PARTITION by Wk order by case Ans10 when '-' then null else Ans10 end desc NULLS last) as RAns10,'''
    sql=sql+''' Wk,ISIN,Jan1,Sem,Mois,mois6,An1,Ans3,Ans5,Ans10 from Stats2 '''

    cur.execute(sql)
    logging.info('''Alimentation de l'historique de la table rank''')




#####Alimentation de la table rank pour la semaine encours
wk=datetime.datetime.now().strftime("%Y%V")[-4:]
logging.info('Purge des données de la semaine de la table rank')
sql="delete from rank where Wk="+" '"+wk+"'"
cur.execute(sql)


#test=conn.execute('SELECT Wk,ISIN FROM Stats2 WHERE ISIN=? and Wk=?', tt)
sql='''insert into rank  select  row_number() over( PARTITION by Wk order by case Jan1 when '-' then null else Jan1 end desc NULLS last) as RJan1,'''
sql=sql+''' row_number() over( PARTITION by Wk order by case Sem when '-' then null else Sem end desc NULLS last) as RSem,'''
sql=sql+''' row_number() over( PARTITION by Wk order by case Mois when '-' then null else Mois end desc NULLS last) as RMois,'''
sql=sql+''' row_number() over( PARTITION by Wk order by case mois6 when '-' then null else mois6 end desc NULLS last) as RMois6,'''
sql=sql+''' row_number() over( PARTITION by Wk order by case An1 when '-' then null else An1 end desc NULLS last) as RAn1,'''
sql=sql+''' row_number() over( PARTITION by Wk order by case Ans3 when '-' then null else Ans3 end desc NULLS last) as RAns3,'''
sql=sql+''' row_number() over( PARTITION by Wk order by case Ans5 when '-' then null else Ans5 end desc NULLS last) as RAns5,'''
sql=sql+''' row_number() over( PARTITION by Wk order by case Ans10 when '-' then null else Ans10 end desc NULLS last) as RAns10,'''
sql=sql+''' Wk,ISIN,Jan1,Sem,Mois,mois6,An1,Ans3,Ans5,Ans10 from Stats2 where Wk='''
sql=sql+"'"+wk+"' order by Wk desc"

print('Requete d insert dans la tabe rank :')
print(sql)

logging.info('Insertion dans la table rank du classement de la semaine : '+str(wk))
cur.execute(sql)
conn.commit()



#############################
##Fermeture page WEB & Base##
#############################



webpage.write('</body>\n</html>' )
webpage.close

conn.close()

stream_handler.setLevel('INFO')
file_handler.setLevel('INFO')

if GlobalWarning==1:
    logging.warning("Le traitement est terminé et contient des erreurs")
else:
    logging.info("Execution terminée")

stream_handler.setLevel(StreamLog)
file_handler.setLevel(FileLog)